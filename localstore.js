const fs = require("fs");

module.exports = (function () {
    "use strict";

    /**
     * Creates a new localstore box
     * @param {String} boxid the id of the box, also the name of the folder
     * in the filesystem.
     * @param {Function} done callback used for async programming, only
     * parameter is an error, in the case that one occured.
     */
    function createBox(boxid, done) {
        if (typeof(boxid) !== "string") {
            done("localstore => create_box: boxid was not a string");
        } else {
            fs.access(`./localstore/${boxid}`, function (err) {
                if (err) {
                    if (err.code === "ENOENT") {
                        fs.mkdir(`./servers/${boxid}`, function (err) {
                            if (err) {
                                done(err);
                            } else {
                                done();
                            }
                        });
                    } else {
                        done(err);
                    }
                } else {
                    done();
                }
            });
        }
    }

    /**
     * Creates a document inside a box, internal function used inside
     * getDocument and setDocument
     * @param {String} boxid the id of the box to create the document in
     * @param {String} docid the id of the document, also its name in the
     * filesystem.
     * @param {Object} body the JS object to write to the file, wil be
     * stringified via JSON.stringify()
     * @param {Function} done callback used for async programming, only
     * parameter is an error, in the case that one occured.
     */
    function createDocument(boxid, docid, body, done) {
        if (typeof(boxid) !== "string") {
            done("localstore => create_document [internal]: boxid was not a string");
        } else if (typeof(docid) !== "string") {
            done("localstore => create_document [internal]: docid was not a string");
        } else if (typeof(body) !== "object") {
            done("localstore => create_document [internal]: body was not an object");
        } else {
            fs.access(`./localstore/${boxid}/${docid}.json`, function (err) {
                if (err) {
                    if (err.code === "ENOENT") {
                        fs.writeFile(`./servers/${boxid}/${docid}.json`, JSON.stringify(body), function (err) {
                            if (err) {
                                done(err);
                            } else {
                                done();
                            }
                        });
                    } else {
                        done(err);
                    }
                } else {
                    done();
                }
            });
        }
    }

    /**
     * Gets a document inside a box, creating an empty one if the specified
     * one does not exist.
     * @param {String} boxid the id of the box to search for the document
     * @param {String} docid the id of the document to search for
     * @param {Function} done callback used for async programming, has two
     * parameters, an error in the case that one occured, and the body
     * of the document requested if no error occured.
     */
    function getDocument(boxid, docid, done) {
        if (typeof(boxid) !== "string") {
            done("localstore => get_document: boxid was not a string");
        } else if (typeof(docid) !== "string") {
            done("localstore => get_document: docid was not a string");
        } else {
            fs.access(`./localstore/${boxid}/${docid}.json`, function (err) {
                if (err) {
                    if (err.code === "ENOENT") {
                        createDocument(boxid, docid, {}, function (err) {
                            if (err) {
                                done(err);
                            } else {
                                done(undefined, {});
                            }
                        });
                    } else {
                        done(err);
                    }
                } else {
                    fs.readFile(`./localstore/${boxid}/${docid}.json`, function (err, data) {
                        if (err) {
                            done(err);
                        } else {
                            done(undefined, JSON.parse(data));
                        }
                    });
                }
            });
        }
    }

    /**
     * Writes a document, if the specified one does not exist, it is created.
     * @param {String} boxid the box to write the document to
     * @param {String} docid the id of the document to write, also its name in the
     * filesystem.
     * @param {Object} body the JS object to write to the file, will be written
     * as a string via JSON.stringify()
     * @param {Function} done callback used for async programming, only
     * parameter is an error, in the case that one occured.
     */
    function setDocument(boxid, docid, body, done) {
        if (typeof(boxid) !== "string") {
            done("localstore => set_document: boxid was not a string");
        } else if (typeof(docid) !== "string") {
            done("localstore => set_document: docid was not a string");
        } else if (typeof(body) !== "object") {
            done("localstore => set_document: body was not anobject");
        } else {
            fs.access(`./localstore/${boxid}/${docid}.json`, function (err) {
                if (err) {
                    if (err.code === "ENOENT") {
                        createDocument(boxid, docid, body, function (err) {
                            if (err) {
                                done(err);
                            } else {
                                done();
                            }
                        });
                    } else {
                        done(err);
                    }
                } else {
                    fs.writeFile(`./localstore/${boxid}/${docid}.json`, JSON.stringify(body), function (err) {
                        if (err) {
                            done(err);
                        } else {
                            done();
                        }
                    });
                }
            });
        }
    }

    return Object.freeze({
        createBox,
        getDocument,
        setDocument
    });
}());
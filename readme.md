# localstore

localstore is a very simple filesystem based database for NodeJS. It works on a basic principle: Boxes & Documents. Documents persist information to a JSON file, you store Documents in Boxes to keep related information together for organisation. 

## Documentation

#### `localstore.createBox(boxid, done)`
Creates a new box to store documents in. 
* `boxid [string]` is the unique identifier for the box, and also serves as the filename of the folder. It must be a string or an error will be thrown. 
* `done [function]` is a callback that is passed an error, if one occured.

``` javascript
const localstore = require("localstore");

// create a new box to store userdata
localstore.createBox("userdata", err => {
    if (err) throw err;
});
```

#### `localstore.setDocument(boxid, docid, body, done)`
Persists an object as a document in a box, will create a document if one does not exist and overwrite a document if it already exists. 
* `boxid [string]` is the id of the box to store the document in, it must be a string or an error will be thrown. 
* `docid [string]` is the unique identifier of the document, as well as it's its filename, it must be a string or an error will be thrown. 
* `body [object]` is the object you want to save to the document, it must be an object or an error will be thrown. 
* `done [function]` is a callback that is passed an error if one occured.

``` javascript
const localstore = require("localstore");

// a user object representing John
var user = {
    name: "John",
    familyname: "Doe",
    age: 17
};

// write the user object to the userdata box
localstore.setDocument("userdata", "john", user, err => {
    if (err) throw err;
});
```

#### `localstore.getDocument(boxid, docid, done)`
Reads a document and parses it into a object that can be used.
* `boxid [string]` is the id of the box to search for the document in it must be a string or an error will be thrown.
* `docid [string]` is the id of the document to search for, it must be a string or an error will be thrown.
* `done [function]` is a callback that will be passed an error if one occured, and the parsed object if the function was sucessful.

``` javascript
const localstore = require("localstore");

// get the data on John and log his name
localstore.getDocument("userdata", "john", (err, data) => {
    if (err) throw err;
    console.log(data.age);
});
```

## How to Use
Download and place a copy of `localstore.js` in your project, and require it as your platform sees fit. This file should not be uploaded or hosted on any other distributions aside from those I release it on, or with my express permission.

In order to use `npm` to install the project, add `gitlab:jordanocokoljic/localstore` to the dependencies in `package.json`, or use `npm install gitlab:jordanocokoljic/localstore`.